import { Component, OnInit } from '@angular/core';

import { MessageService } from '../message.service';
import { interval } from 'rxjs';
import { Message } from '../message';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css'],
})
export class MessageComponent implements OnInit {
  messages: Message[] = [];
  topics: String[] = [];


  constructor(private messageService: MessageService) { }

  ngOnInit(): void {
    this.getAllMessages();
    this.getAllTopics();

  }
  public insertMessage(data: any) {
    let date: Date = new Date();
    let msg: Message = new Message(data.message_field, date.toLocaleString(), "");

    this.messageService.putMessages(data.topic_field, msg).subscribe();
    setTimeout(() => {
      this.getAllMessages();
    }, 3000)

  }

  ngAfterViewInit() {
    //interval(3000).subscribe(() => this.getAllMessages());
  }

  public getAllMessages(): void {
    this.messageService.getMessages().subscribe((data: Message[]) => {
      this.messages = data.slice().reverse();
      console.log("Triggered")
    });
  }

  public getAllTopics() {
    this.messageService.getTopics().subscribe(
      (data: String[]) => this.topics = data
    )
  }
}
