import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Message } from './message';
import { Observable, Subject, tap } from 'rxjs';

@Injectable({
  providedIn: 'root',
})

export class MessageService {
  private baseUrl: string = 'http://localhost:8888/postings/';

  public getLink() {
    return this.baseUrl;
  }
  constructor(private http: HttpClient) { }

  // public putMessages(message: string) {
  //   return this.http.post(`${this.baseUrl}`, message);
  // }

  public putMessages(topic: String, message: Message) {
    let url: string = this.baseUrl + topic;
    return this.http.post(url, message);
  }

  getTopics(): Observable<String[]> {
    return this.http
      .get<String[]>(`${this.baseUrl}` + "topics")
      .pipe(
        tap((data) =>
          console.warn('taaaaaaaaap executed: ', JSON.stringify(data))
        )
      );
  }

  getMessages(): Observable<Message[]> {
    return this.http
      .get<Message[]>(`${this.baseUrl}`)
      .pipe(
        tap((data) =>
          console.warn('taaaaaaaaap executed: ', JSON.stringify(data))
        )
      );
  }
}
