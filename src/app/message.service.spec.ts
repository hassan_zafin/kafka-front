import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MessageService } from './message.service';
import { Message } from './message';
import { HttpResponse } from '@angular/common/http';



describe('GETMessageService', () => {
  let service: MessageService;
  let httpMock : HttpTestingController;

  beforeEach(() => {
    
    TestBed.configureTestingModule(
      {
        imports: [ HttpClientTestingModule ]
      
      }
      );
    service = TestBed.inject(MessageService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  })

  //Test 1 ========================

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  //Test 2 ========================

  it('should retreive data from GET API', () => {
    const eventsData : Message[] = [ {
      content:'hello1',
      date:'date1'
    },
    {
      content : 'hello2',
      date : 'date2'
    }];

    service.getMessages().subscribe(realData => {
      expect(realData.length).toBe(2);
      console.warn("==================");
      console.warn(realData);
      console.warn("==================");
      expect(realData).toEqual(eventsData);
    })


    const myrequest = httpMock.expectOne(`${service.getLink()}`);
    expect(myrequest.request.method).toBe('GET');
    
    myrequest.flush(eventsData);
}) ;

  //Test 3 ========================
  it('should create a post', () => {

    const eventToCreate : Message = {content : "Hey Im a new eventtt", date : "this is my birth date"};

    service.putMessages(eventToCreate).subscribe(
      data => {
        expect(data).toEqual(eventToCreate);
        console.log(data)
      } 
    );
    
    const myrequest = httpMock.expectOne(`${service.getLink()}`);
    expect(myrequest.request.method).toBe('POST');
    expect(myrequest.request.body).toEqual(eventToCreate);

    const expectedResponse = new HttpResponse(
      {status: 201, statusText: 'Created', body: eventToCreate}
    );
      myrequest.event(expectedResponse);
  }) ;


  
});
