import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router'
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MessageComponent } from './message/message.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  imports: [BrowserModule, RouterModule, HttpClientModule, FormsModule, AppRoutingModule, FontAwesomeModule],
  declarations: [AppComponent, MessageComponent, DashboardComponent],
  bootstrap: [AppComponent],
  providers: [HttpClientModule]
})
export class AppModule {




}
