import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject, tap } from 'rxjs';

@Injectable({
    providedIn: 'root',
})

export class TopicService {

    private baseUrl: string = 'http://localhost:8888/postings/topics/';

    public getLink() {
        return this.baseUrl;
    }
    constructor(private http: HttpClient) { }



    public getTopics() {
        return this.http.get(`${this.baseUrl}`);
    }
    public getPartitions(topicName: String) {
        return this.http.get(`${this.baseUrl}` + topicName);
    }
    public createTopic(name: string, partitions: any, replicas: any) {
        return this.http.get('http://localhost:8888/postings/topics/' + name + "/" + replicas + "/" + partitions);

    }




}
