import { Component, OnInit } from '@angular/core';
import { timeout } from 'rxjs';
import { tap } from 'rxjs';
import { TopicService } from './topic.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  topics: string[] = [];
  partitions: string[] = [];
  constructor(private topicService: TopicService) { }

  ngOnInit(): void {
    this.topicService.getTopics().subscribe(
      (data: any) => {
        console.log(data);
        this.topics = data;
      });
  }

  public getPartition(topic: String): any {
    this.topicService.getPartitions(topic).subscribe(
      (data: any) => { this.partitions = data }
    );

  }

  public createTopic(data: any): any {
    this.topicService.createTopic(data.name, data.partitions, data.replicas).subscribe(
      () => {
        setTimeout(() => {
          this.refreshTopics();
        }, 3000);

      }
    )
  }

  public refreshTopics() {
    this.topicService.getTopics().subscribe(
      (data: any) => {
        this.topics = data;
      });
  }




}