export class Message {
  content: String = '';
  date: String = '';
  topic: String = '';

  constructor(c: String, d: String, e: String) {
    this.content = c;
    this.date = d;
    this.topic = e;
  }


}
